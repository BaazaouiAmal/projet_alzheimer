import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AuthGuard } from "../../shared/guard/auth.guard"

// Include components for in which router service to be used
import { SignInComponent } from '../../components/sign-in/sign-in.component';
import { SignUpComponent } from '../../components/sign-up/sign-up.component';
import { DashboardComponent } from '../../components/dashboard/dashboard.component';
import { ForgotPasswordComponent } from '../../components/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from '../../components/verify-email/verify-email.component';
// Include components for in which router service to be used
import { AddUserComponent } from '../../components/add-user/add-user.component';
import { UsersListComponent } from '../../components/users-list/users-list.component';
import { EditUserComponent } from '../../components/edit-user/edit-user.component';
import { GreenHouseComponent } from "../../components/green-house/green-house.component";




// Routes array define component along with the path name for url
const routes: Routes = [
  { path: '', redirectTo: '/sign-in', pathMatch: 'full'},
  { path: 'sign-in', component: SignInComponent},
  { path: 'register-user', component: SignUpComponent},
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'value',component: GreenHouseComponent},
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'verify-email-address', component: VerifyEmailComponent },
  { path: 'register-userlist', component: AddUserComponent },
  { path: 'view-users', component: UsersListComponent },
  { path: 'edit-user/:id', component: EditUserComponent }
];

@NgModule({
  imports: [CommonModule,RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
