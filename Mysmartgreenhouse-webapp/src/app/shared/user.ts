export interface User {
 $key: string;
 fullName: string;
 email: string;
 phone: Number;
}