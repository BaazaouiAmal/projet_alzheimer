import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// Import below modules for NGX Toastr
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// Reactive Form
import { ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
// App routing modules
import { AppRoutingModule } from './shared/routing/app-routing.module';
// Firebase Modules
import { AngularFireDatabaseModule } from '@angular/fire/database';
// App components
import { AppComponent } from './app.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './components/verify-email/verify-email.component';
// Include components for in which router service to be used
import { AddUserComponent } from './components/add-user/add-user.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { GreenHouseComponent } from "./components/green-house/green-house.component";

import {DataTablesModule} from 'angular-datatables';


// Firebase services + enviorment module
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
// Auth service
import { AuthService } from "./shared/services/auth.service";


@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    SignUpComponent,
    DashboardComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    AddUserComponent,
    EditUserComponent,
    UsersListComponent,
    GreenHouseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
     AngularFireDatabaseModule,
    ToastrModule.forRoot ({ timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,     }),    // Register NgxToast NPM module
    ReactiveFormsModule, // Reactive forms module
    BrowserAnimationsModule,// required animations module
    NgxPaginationModule,  // Include it in imports array
    DataTablesModule

  ],
  
  providers: [AuthService],
  bootstrap: [AppComponent]
})

export class AppModule { }