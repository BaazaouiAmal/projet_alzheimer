export interface Value {
  $key:string;
  Localisation: string;
  Distance:string;
  Pulsation:string;
  rythmecardiaque:string;
}